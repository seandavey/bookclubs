# Bookclubs

a site for managing multiple book clubs

- history of books nominated and read
- voting on which books to read
- current reading schedule
- rotation list of nominators
- Slack-like discussions