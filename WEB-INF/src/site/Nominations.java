package site;

import java.sql.Types;
import java.util.List;

import app.Module;
import app.Request;
import app.Site;
import app.Site.Message;
import app.Subscriber;
import db.DBConnection;
import db.FormHook;
import db.ManyToMany;
import db.NameValuePairs;
import db.OrderBy;
import db.QueryAdjustor;
import db.Rows;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Tabs;
import mail.Mail;
import pages.Page;
import ui.Card;
import ui.Table;
import util.Text;
import web.HTMLWriter;

public class Nominations extends Module implements Subscriber {
	@Override
	protected void adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("nominations")
			.add(new JDBCColumn("clubs"))
			.add(new JDBCColumn("books_id", Types.INTEGER))
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"));
		db.createManyToManyLinkTable("nominations", "books");
		JDBCTable.adjustTable(table_def, true, true, db);
		table_def = new JDBCTable("nominations_votes")
			.add(new JDBCColumn("books"))
			.add(new JDBCColumn("nominations"))
			.add(new JDBCColumn("people"));
		JDBCTable.adjustTable(table_def, true, true, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request r) {
		if (super.doGet(r))
			return true;
		if ("current".equals(r.getPathSegment(1))) {
			write(r);
			writeNextNominators(r);
			return true;
		}
		int nominations_id = r.getPathSegmentInt(1);
		if (nominations_id == 0)
			return false;
		String segment_two = r.getPathSegment(2);
		if ("books".equals(segment_two)) {
			r.w.write("<option></option>");
			Rows rows = new Rows(new Select("id,title").from("nominations_books").joinOn("books", "books.id=books_id").whereEquals("nominations_id", nominations_id).orderBy("lower(title)"), r.db);
			while (rows.next())
				r.w.write("<option value=\"")
					.write(rows.getInt(1))
					.write("\">")
					.write(HTMLWriter.encode(rows.getString(2)))
					.write("</option>");
			rows.close();
			return true;
		}
		int people_id = r.getUser().getId();
		writeNomination(
			nominations_id,
			r.db.lookupInt(new Select("clubs_id").from("nominations").whereIdEquals(nominations_id), 0),
			true,
			r.db.lookupInt(new Select("books_id").from("nominations_votes").whereEquals("nominations_id", nominations_id).andEquals("people_id", people_id), 0),
			r);
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch(segment_one) {
		case "vote":
			int books_id = r.getPathSegmentInt(3);
			int nominations_id = r.getPathSegmentInt(2);
			int people_id = r.getUser().getId();
			NameValuePairs nvp = new NameValuePairs()
				.set("books_id", books_id)
				.set("nominations_id", nominations_id)
				.set("people_id", people_id);
			r.db.updateOrInsert("nominations_votes", nvp, "nominations_id=" + nominations_id + " AND people_id=" + people_id);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Site.pages.add(new Page("Nominations", false) {
			@Override
			public void
			writeContent(Request r) {
				r.w.js("""
					function update_books(t){
						let f=_.$('#nomination')
						_.ui.set_options(f.querySelector('#books_id'),'books_id',t,f.querySelector('#books_id_row').lastChild)
					}
					app.subscribe('nominations books',function(){
						net.get_text(context+'/Nominations/'+app.get('view:nominations').id+'/books',update_books)
					})
					app.subscribe('nominations_books',function(){
						net.get_text(context+'/Nominations/'+app.get('view:nominations').id+'/books',update_books)
					})""");
				ViewState.setBaseFilter("nominations", "EXISTS(SELECT 1 FROM clubs_people WHERE nominations.clubs_id=clubs_people.clubs_id AND people_id=" + r.getUser().getId() + ")", r);
				Site.site.newView("nominations", r).writeComponent();
			}
		}, db);
		Site.site.addUserDropdownItem(new Page("Edit Votes", false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("nominations_votes", r).writeComponent();
			}
		}.addRole("administrator"));
		Site.site.subscribe("nominations_votes", this);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		switch(name) {
		case "nominations":
			return new ViewDef(name)
				.addFormHook(new FormHook() {
					@Override
					public void
					afterForm(int id, Rows data, ViewDef view_def, Mode mode, boolean printer_friendly, Request r) {
						r.w.js("function set_club(){let e=_.$('#clubs_id');if(e)net.post(context+'/session','clubs_id='+e.options[e.selectedIndex].value)};set_club()");
						if (r.db.lookupBoolean(new Select("books_id IS NULL").from("nominations").whereIdEquals(id)))
							return;
						Rows rows = new Rows(new Select("first,last,title").from("nominations_votes").joinOn("people", "people_id=people.id").joinOn("books", "books_id=books.id").whereEquals("nominations_id", id).orderBy("1,2"), r.db);
						if (!rows.isBeforeFirst()) {
							rows.close();
							return;
						}
						r.w.h5("Votes");
						Table t = r.w.ui.table().addDefaultClasses();
						while (rows.next())
							t.tr()
								.td(rows.getString(1) + " " + rows.getString(2))
								.td(rows.getString(3));
						rows.close();
						t.close();
					}
				})
				.addSection(new Tabs("clubs_id", new OrderBy("(SELECT name FROM clubs WHERE clubs.id=clubs_id)")).setPluralize(false))
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().edit().delete().view())
				.setDefaultOrderBy("date DESC")
				.setColumnNamesForm("clubs_id", "books_id", "_owner_", "date", "notes")
				.setColumnNamesTable("date", "books_id", "_owner_", "notes")
				.setColumn(new LookupColumn("books_id", "books", "title").setAllowNoSelection(true).setDisplayName("book chosen").setQueryAdjustor(new QueryAdjustor() {
					@Override
					public void
					adjust(Select query, View v, Request r) {
						Mode mode = v.getMode();
						if (mode == Mode.ADD_FORM)
							query.where("false");
						else if (mode == Mode.EDIT_FORM)
							query.joinOn("nominations_books", "books.id=books_id AND nominations_id=" + v.data.getInt("id"));
					}
				}))
				.setColumn(new LookupColumn("clubs_id", "clubs", "name")
					.setOnChange("set_club()")
					.setQueryAdjustor(new QueryAdjustor() {
						@Override
						public void
						adjust(Select query, View v, Request r) {
							query.andWhere("id IN (SELECT clubs_id FROM clubs_people WHERE people_id=" + r.getUser().getId() + ")");
						}
					})
				)
				.setColumn(new Column("date").setDefaultToDateNow().setIsRequired(true))
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDefaultToUserId().setDisplayName("posted by"))
				.addRelationshipDef(new ManyToMany("nominations books", "nominations_books", "title")
					.setAllowAdding(true)
					.setQueryAdjustor(new QueryAdjustor() {
						@Override
						public void
						adjust(Select query, View v, Request r) {
							int clubs_id = r.getSessionInt("clubs_id", 0);
							if (clubs_id != 0)
								query.whereEquals("clubs_id", clubs_id);
						}
					}));
		case "nominations books":
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setDefaultOrderBy("LOWER(title)")
				.setFrom("books")
				.setColumnNamesTable("title", "author");
		case "nominations_votes":
			return new ViewDef(name)
				.setDefaultOrderBy("id DESC")
				.setRecordName("Vote", true)
				.setColumnNamesTable("books_id", "people_id")
				.setColumn(new LookupColumn("books_id", "books", "title").setDisplayName("book"))
				.setColumn(new LookupColumn("people_id", "people", "first,last").setDisplayName("person"));
		}
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, DBConnection db) {
		if (message == Message.INSERT) {
			int vote_id = ((Integer)data).intValue();
			int nominations_id = db.lookupInt(new Select("nominations_id").from("nominations_votes").whereIdEquals(vote_id), 0);
			int clubs_id = db.lookupInt(new Select("clubs_id").from("nominations").whereIdEquals(nominations_id), 0);
			String club = db.lookupString(new Select("name").from("clubs").whereIdEquals(clubs_id));
			int num_votes = db.lookupInt(new Select("COUNT(1)").from("nominations_votes").whereEquals("nominations_id", nominations_id), 0);
			int num_people = db.lookupInt(new Select("COUNT(1)").from("clubs_people").whereEquals("clubs_id", clubs_id), 0);
			if (num_votes >= num_people - 1) {
				int winner = db.lookupInt(new Select("books_id").from("nominations_votes").whereEquals("nominations_id", nominations_id).groupBy("books_id").orderBy("count(1) DESC"), 0);
				if (winner != 0) {
					db.update("nominations", "books_id=" + winner, nominations_id);
					new Mail(club + " Book Club Vote", "The book chosen for " + club + " Book Club by vote is " + db.lookupString(new Select("title").from("books").whereIdEquals(winner)) + " by " + db.lookupString(new Select("author").from("books").whereIdEquals(winner)) + ".")
						.to(db.readValues(new Select("email").from("people").joinOn("clubs_people", "people.id=people_id").whereEquals("clubs_id", clubs_id)))
						.send();
				}
			}
		}
	}

	//--------------------------------------------------------------------------

	static void
	writeNextNominators(Request r) {
		Rows clubs = new Rows(new Select("id,name").from("clubs").where("EXISTS(SELECT 1 FROM clubs_people WHERE clubs.id=clubs_people.clubs_id AND people_id=" + r.getUser().getId() + ")").orderBy("name"), r.db);
		if (!clubs.isBeforeFirst()) {
			clubs.close();
			return;
		}
		Card card = r.w.ui.card();
		r.w.addClass("text-bg-secondary");
		card.header("Next Nominators")
			.textOpen();
		Table t = r.w.ui.table().addDefaultClasses();
		while (clubs.next()) {
			t.tr().td();
			r.w.write("<strong>").write(clubs.getString("name")).write("</strong>");
			int nominator = r.db.lookupInt(new Select("_owner_").from("nominations").whereEquals("clubs_id", clubs.getInt("id")).orderBy("date DESC"), 0);
			if (nominator != 0) {
				List<Integer> ids = r.db.readValuesInt(new Select("people_id").from("clubs_people").whereEquals("clubs_id", clubs.getInt("id")).orderBy("_order_"));
				for (int i=0; i<ids.size(); i++) {
					int id = ids.get(i);
					if (id == nominator) {
						if (i < ids.size() - 1)
							t.td(r.db.lookupString(new Select("first,last").from("people").whereIdEquals(ids.get(i + 1))));
						else
							t.td(r.db.lookupString(new Select("first,last").from("people").whereIdEquals(ids.get(0))));
						break;
					}
				}
			} else {
				String first = r.db.lookupString(new Select("first,last").from("people").join("people", "clubs_people").whereEquals("clubs_id", clubs.getInt("id")).orderBy("_order_"));
				if (first != null)
					t.td(first);
			}
		}
		t.close();
		card.close();
		clubs.close();
	}

	//--------------------------------------------------------------------------

	static void
	writeNomination(int nominations_id, int clubs_id, boolean use_voting, int current_vote, Request r) {
		int num_votes = 0;
		int num_people = 0;
		boolean show_votes = false;
		int num_columns = 2;
		if (use_voting) {
			num_columns++;
			num_votes = r.db.countRows(new Select().from("nominations_votes").whereEquals("nominations_id", nominations_id));
			num_people = r.db.countRows(new Select().from("clubs_people").whereEquals("clubs_id", clubs_id));
			show_votes = num_votes >= num_people - 1 || r.userIsAdmin();
			if (show_votes)
				num_columns++;
		}
		String select = "books.id,title,author,notes";
		if (show_votes)
			select += ",(SELECT COUNT(1) FROM nominations_votes WHERE nominations_votes.nominations_id=" + nominations_id + " AND nominations_votes.books_id=nominations_books.books_id) votes";
		Rows books = new Rows(
			new Select(select)
				.from("nominations_books")
				.joinOn("books", "books.id=books_id")
				.whereEquals("nominations_id", nominations_id)
				.orderBy("title"),
		r.db);
		Table t = r.w.ui.table().addDefaultClasses();
		while (books.next()) {
			t.tr().td(books.getString("title")).td(books.getString("author"));
			if (use_voting) {
				if (show_votes) {
					int votes = books.getInt("votes");
					if (votes == 0)
						t.td();
					else
						t.td(votes + " " + Text.pluralize("vote", votes));
				}
				int books_id = books.getInt("id");
				if (current_vote == books_id)
					t.td("my vote");
				else {
					t.td();
					r.w.ui.buttonOnClick("Vote", "let c=this.closest('div');net.post(context+'/Nominations/vote/" + nominations_id + "/" + books_id + "',null,function(){net.replace(c,context+'/Nominations/" + nominations_id + "')})");
				}
			}
			String notes = books.getString("notes");
			if (notes != null)
				t.tr().td(num_columns, notes);
		}
		books.close();
		t.close();
		if (use_voting) {
			if (num_votes < num_people)
				num_people--;
			r.w.addClass("mt-3")
				.p(num_votes + " " + Text.pluralize("person", num_votes) + " of " + num_people + " have voted");
		}
	}

	//--------------------------------------------------------------------------

	static void
	write(Request r) {
		Rows rows = new Rows(
			new Select("name,nominations.id,first,last,use_voting,clubs.id clubs_id")
			.from("clubs")
			.leftJoin("clubs", "nominations")
			.joinOn("people", "people.id=_owner_")
			.where("EXISTS(SELECT 1 FROM clubs_people WHERE clubs.id=clubs_people.clubs_id AND people_id=" + r.getUser().getId() + ")")
			.andWhere("books_id IS NULL")
			.andWhere("rotate_nominations")
			.orderBy("name"),
		r.db);
		while (rows.next()) {
			Card card = r.w.ui.card();
			r.w.addClass("text-bg-info");
			card.header(rows.getString(1) + " Nominations")
				.title(rows.getString(3) + " " + rows.getString(4))
				.textOpen();
			boolean use_voting = rows.getBoolean(5);
			int nominations_id = rows.getInt(2);
			writeNomination(
				nominations_id,
				rows.getInt(6),
				use_voting,
				use_voting ? r.db.lookupInt(new Select("books_id").from("nominations_votes").whereEquals("nominations_id", nominations_id).andEquals("people_id", r.getUser().getId()), 0) : 0,
				r);
			card.close();
		}
		rows.close();
	}
}
