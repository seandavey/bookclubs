package site;

import java.sql.Types;
import java.util.Map;

import app.Module;
import app.Request;
import app.Site;
import db.DBConnection;
import db.OneToMany;
import db.OrderBy;
import db.QueryAdjustor;
import db.Rows;
import db.Select;
import db.View;
import db.ViewDef;
import db.ViewState;
import db.column.Column;
import db.column.DateColumn;
import db.column.LookupColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Tabs;
import pages.Page;
import ui.Card;
import ui.Table;
import util.Time;

public class Books extends Module {
	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("books")
			.add(new JDBCColumn("author", Types.VARCHAR))
			.add(new JDBCColumn("clubs"))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("title", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, true, db);
		table_def = new JDBCTable("books_dates")
			.add(new JDBCColumn("books"))
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("description", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, true, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(Request r) {
		if (super.doGet(r))
			return true;
		if ("current".equals(r.getPathSegment(1))) {
			writeCurrentBooks(r);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Site.pages.add(new Page("Books", false) {
			@Override
			public void
			writeContent(Request r) {
				ViewState.setBaseFilter("edit books", "EXISTS(SELECT 1 FROM clubs_people WHERE books.clubs_id=clubs_people.clubs_id AND people_id=" + r.getUser().getId() + ")", r);
				Site.site.newView("edit books", r).writeComponent();
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		switch(name) {
		case "books":
		case "edit books":
			LookupColumn clubs_column = new LookupColumn("clubs_id", "clubs", "name")
				.setQueryAdjustor(new QueryAdjustor() {
					@Override
					public void
					adjust(Select query, View v, Request r) {
						query.andWhere("id IN (SELECT clubs_id FROM clubs_people WHERE people_id=" + r.getUser().getId() + ")");
					}
				});
			if (name.equals("books"))
				clubs_column.setDefaultToSessionAttribute();
			return new ViewDef(name)
				.addSection(new Tabs("clubs_id", new OrderBy("(SELECT name FROM clubs WHERE clubs.id=clubs_id)")).setPluralize(false))
				.setDefaultOrderBy("lower(title)")
				.setFrom("books")
				.setColumnNamesForm("clubs_id", "title", "author", "notes")
				.setColumnNamesTable("title", "author", "nominations", "chosen")
				.setColumn(new DateColumn("chosen") {
					@Override
					public boolean
					writeValue(View v, Map<String, Object> data, Request r) {
						try (Rows rows = new Rows(new Select("date").from("nominations").whereEquals("books_id", v.data.getInt("id")).orderBy("1"), r.db)) {
							while (rows.next()) {
								if (!rows.isFirst())
									r.w.write(", ");
								r.w.write(Time.formatter.getDateShort(rows.getDate(1)));
							}
						}
						return true;
					}
				}.setIsSortable(false))
				.setColumn(clubs_column)
				.setColumn(new Column("nominations") {
					@Override
					public boolean
					writeValue(View v, Map<String, Object> data, Request r) {
						try (Rows rows = new Rows(new Select("first,last,date").from("nominations").joinOn("people", "_owner_=people.id").where("EXISTS(SELECT 1 FROM nominations_books WHERE nominations_id=nominations.id AND books_id=" + v.data.getInt("id") + ")").orderBy("3"), r.db)) {
							while (rows.next()) {
								if (!rows.isFirst())
									r.w.write(", ");
								r.w.write(rows.getString(1)).space().write(rows.getString(2)).write(" <span style=\"text-size:x-small\">(").write(Time.formatter.getDateShort(rows.getDate(3))).write(")</span>");
							}
						}
						return true;
					}
				}.setIsSortable(false))
				.addRelationshipDef(new OneToMany("books_dates"));
		case "books_dates":
			return new ViewDef(name)
				.setDefaultOrderBy("date")
				.setRecordName("Date", true);
		}
		return null;
	}

	//--------------------------------------------------------------------------

	static void
	writeCurrentBooks(Request r) {
		Rows books = new Rows(
			new Select("(SELECT name FROM clubs WHERE clubs.id=clubs_id) AS name,title,author,id,(SELECT date FROM books_dates WHERE books.id=books_id LIMIT 1) as date")
				.from("books")
				.where("EXISTS(SELECT 1 FROM clubs_people WHERE books.clubs_id=clubs_people.clubs_id AND people_id=" + r.getUser().getId() + ")")
				.andWhere("EXISTS(SELECT 1 FROM books_dates WHERE books_id=books.id AND date>=CURRENT_DATE)")
				.orderBy("5,1"),
			r.db);
		while (books.next()) {
			Card card = r.w.ui.card();
			r.w.addClass("text-bg-success");
			card.header("Reading for " + books.getString("name"))
				.title(books.getString("title"))
				.subtitle(books.getString("author"))
				.textOpen();
			Table t = r.w.ui.table().addDefaultClasses();
			try (Rows books_dates = new Rows(new Select("*").from("books_dates").whereEquals("books_id", books.getInt("id")).andWhere("date>=CURRENT_DATE").orderBy("date"), r.db)) {
				while (books_dates.next())
					t.tr().td(Time.formatter.getDateShort(books_dates.getDate("date"))).td(books_dates.getString("description"));
			}
			t.close();
			card.close();
		}
		books.close();
	}

}
