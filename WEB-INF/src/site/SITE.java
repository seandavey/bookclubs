package site;

import java.io.IOException;
import java.sql.Types;

import app.EditSite;
import app.People;
import app.PeopleViewDef;
import app.Request;
import app.Site;
import db.DBConnection;
import db.OneToMany;
import db.Reorderable;
import db.ViewDef;
import db.access.AccessPolicy;
import db.column.LookupColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import pages.Page;
import sitemenu.SiteMenu;
import social.News;
import social.NewsFeed;
import web.Head;

public class SITE extends Site {
	public
	SITE(String base_directory) {
		super(base_directory, "Home", "site");
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(HttpServletRequest http_request, HttpServletResponse http_response) throws IOException {
		String path = http_request.getServletPath();
		if (path.equals("/session")) {
			http_request.getSession().setAttribute("clubs_id", Integer.valueOf(http_request.getParameter("clubs_id")));
			return true;
		}
		return super.doPost(http_request, http_response);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		addModule(new Books(), db);
		addModule(new EditSite(), db);
		News news = new News();
		addModule(news, db);
		NewsFeed news_feed = new NewsFeed();
		addModule(news_feed, db);
		news.addProvider(news_feed, db);
		addModule(new Nominations(), db);
		addModule(new People()
			.addJDBCColumn(new JDBCColumn("send_email_when_someone_posts_to_the_site", Types.BOOLEAN).setDefaultValue(true))
			.addJDBCColumn(new JDBCColumn("email_updates_to_my_conversations", Types.BOOLEAN).setDefaultValue(true))
		, db);
		addModule(new SiteMenu(), db);

		JDBCTable table_def = new JDBCTable("clubs")
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("rotate_nominations", Types.BOOLEAN))
			.add(new JDBCColumn("use_voting", Types.BOOLEAN));
		JDBCTable.adjustTable(table_def, true, true, db);
		table_def = new JDBCTable("clubs_people")
			.add(new JDBCColumn("clubs_id", Types.INTEGER))
			.add(new JDBCColumn("people_id", Types.INTEGER))
			.add(new JDBCColumn("_order_", Types.INTEGER));
		JDBCTable.adjustTable(table_def, true, true, db);

		pages.add(new Page("Home", false) {
			@Override
			public void
			writeContent(Request r) {
				r.w.write("<div style=\"display:flex;flex-wrap:wrap;gap:20px\">")
					.ui.buttonOnClick("Reading Schedule", "new Dialog({title:'Reading Schedule',url:context+'/Books/current'}).open()", false, "success")
					.ui.buttonOnClick("Current Nominations", "new Dialog({title:'Current Nominations',url:context+'/Nominations/current'}).open()", false, "info")
					.write("</div>");
				NewsFeed n = (NewsFeed) Site.site.getModule("NewsFeed");
				if (n.isActive())
					n.write(r);
			}
		}, db);
		addUserDropdownItem(new Page("Edit Clubs", false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("clubs", r).writeComponent();
			}
		}.addRole("administrator"));
	}

	//--------------------------------------------------------------------------

	@Override
	protected ViewDef
	newViewDef(String name) {
		switch(name) {
		case "clubs":
			return new ViewDef(name)
				.setDefaultOrderBy("name")
				.addRelationshipDef(new OneToMany("clubs_people"));
		case "clubs_people":
			return new ViewDef(name)
				.setDefaultOrderBy("_order_")
				.setRecordName("person", true)
				.setReorderable(new Reorderable())
				.setColumnNamesTable("people_id")
				.setColumn(new LookupColumn("people_id", "people", "first,last", "active", "first,last"));
		case "people":
			return new ViewDef(name)
				.setDefaultOrderBy("first,last")
				.setColumnNamesTable("first", "last");
		case "person settings":
			return new PeopleViewDef(name, new AccessPolicy().edit(), 40)
				.setFrom("people")
				.setColumnNamesForm("user_name", "password", "email_updates_to_my_conversations", "send_email_when_someone_posts_to_the_site");
		}
		return super.newViewDef(name);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	pageOpen(String current_page, Head head, boolean open_main, Request r) {
		if (head != null)
			head.styleSheet("flatpickr.min", false).close();
		SiteMenu site_menu = (SiteMenu)getModule("SiteMenu");
		if (site_menu != null) {
			site_menu.write(current_page, r);
			r.w.setId("page").addStyle("flex-grow:1").tagOpen("div");
		}
		if (open_main)
			r.w.mainOpen();
	}
}
